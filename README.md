# projet-mobile

L'objectif de ce projet est de faire une application mobile en utilisant NativeScript-Angular et Symfony.

Le thème du projet est libre (à faire valider auprès d'un formateur si pas un des projets exemple). Dans les projets qu'on vous propose vous pouvez :

* Faire une application de vente d'objet type leboncoin (juste une liste d'objet en vente, un compte utilisateur.ice et la possibilité de rajouter de nouveaux objets à vendre.)
* Faire une application de scan de code barre et qui affiche le produit qui correspond et éventuellement si on le "possède" déjà ou non (si vous utilisez une api externe, bien vérifier avant comme celle ci fonctionne avant de vous lancer)

D'autres projets sont possibles, s'ils utilisent une ou des fonctionnalités propre au mobile (appareil photo, géolocalisation, accéléromètre), c'est pas mal, mais pas absolument obligatoire selon le projet.

Dans tous les cas, si vous avez un backend, il sera fait en Symfony 4 en REST.

Vous pouvez faire ce projet seul.e ou à deux.